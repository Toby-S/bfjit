
///! A cross-platform abstraction over memory-related functions of use to a JIT
///! compiler.

use std::io::{Error, self};

pub use self::memory::MemoryProtection;

#[derive(Debug, Eq, Hash, PartialEq)]
#[allow(raw_pointer_derive)]
pub struct Memory {

    /// The address of the first byte of allocated memory. Will never be a null pointer.
    address: *mut usize,

    /// The amount of allocated memory, in bytes.
    length: usize,

}

impl Memory {

    /// Attempts to allocate the specified amount of pages. The memory will be readable and
    /// writable, but not executable.
    ///
    /// # Arguments
    /// * `count` The amount of pages to allocate.
    ///
    /// # Returns
    /// * The `Result` containing either the `Memory` or the `Error` raised when attempting to
    ///   allocate.
    pub fn allocate(count: usize) -> io::Result<Memory> {
        memory::allocate(count).map(|address| Memory::new(address, count * *memory::PAGE_SIZE))
    }

    /// Creates a new `Memory`.
    ///
    /// # Arguments
    /// * `address` The address of the first byte of allocated memory.
    /// * `length` The length of allocated memory, in bytes.
    fn new(address: *mut usize, length: usize) -> Memory {
        Memory { address: address, length: length }
    }

    /// Gets the address of the first byte of allocated memory.
    pub fn address(&self) -> *mut usize {
        self.address
    }

    /// Gets the length of allocated memory, in bytes.
    pub fn length(&self) -> usize {
        self.length
    }

    /// Applies the specified `MemoryProtection`.
    ///
    /// The `MemoryProtection` will be applied to all of the memory.
    ///
    /// # Arguments
    /// * `protection` The `MemoryProtection` to apply. Existing protections will be revoked.
    ///
    /// # Returns
    /// * The `Result` containing `()` if the protection succeeded, the `Error` otherwise.
    pub fn protect(&mut self, protection: MemoryProtection) -> io::Result<()> {
        if memory::protect(self, protection) { Ok(()) } else { Err(Error::last_os_error()) }
    }

}

impl Drop for Memory {

    fn drop(&mut self) {
        unsafe { memory::deallocate(self); }
    }

}

/// Contains windows-specific memory functions used by the JIT.
#[cfg(windows)]
mod memory {

    extern crate kernel32;
    extern crate winapi;

    use std::{io, ptr};
    use std::io::Error;
    use std::os::raw;

    use self::winapi::sysinfoapi::{LPSYSTEM_INFO, SYSTEM_INFO};
    use self::winapi::basetsd::SIZE_T;
    use self::winapi::minwindef::{DWORD, LPVOID, TRUE};
    use self::winapi::winnt;

    use super::Memory;

    /// The null value, indicating a call to `VirtualAlloc` was unsuccessful.
    const NULL: usize = 0;

    lazy_static! {
        /// The size of an entry in the page table, in bytes.
        pub static ref PAGE_SIZE: usize = {
            let (a, b) = (0, 0);
            let (min, max) = (a as *mut raw::c_void, b as *mut raw::c_void);

            let mut info = SYSTEM_INFO {
                wProcessorArchitecture: 0, wReserved: 0, dwPageSize: 0,
                lpMinimumApplicationAddress: min, lpMaximumApplicationAddress: max,
                dwActiveProcessorMask: 0, dwNumberOfProcessors: 0, dwProcessorType: 0,
                dwAllocationGranularity: 0, wProcessorLevel: 0, wProcessorRevision: 0,
            }; // TODO really???

            unsafe { kernel32::GetSystemInfo(&mut info as LPSYSTEM_INFO); }

            info.dwPageSize as usize
        };
    }

    /// Applies the specified `MemoryProtection` to the specified region of memory.
    ///
    /// The `MemoryProtection` will be applied to all of the memory in the interval
    /// [address, address + length - 1].
    ///
    /// # Arguments
    /// * `memory` The `Memory` to protect.
    /// * `protection` The `MemoryProtection` to apply. Existing protections will be revoked.
    ///
    /// # Returns
    /// `true` iff the call was successful.
    pub fn protect(memory: &mut Memory, protection: MemoryProtection) -> bool {
        let mut old_protection = 0u32;
        let pointer = &mut old_protection as *mut u32 as DWORD;

        let (address, length) = (memory.address() as LPVOID, memory.length() as SIZE_T);
        unsafe { kernel32::VirtualProtect(address, length, protection as DWORD, pointer) == TRUE }
    }

    /// A state of protection for a region of memory, on a windows machine.
    #[derive(Clone, Copy, Eq, Hash, PartialEq)]
    pub enum MemoryProtection {

        /// Specifies that the region of memory can be read from.
        Read = winnt::PAGE_READONLY as isize,

        /// Specifies that the region of memory can be written to.
        Write = winnt::PAGE_WRITECOPY as isize,

        /// Specifies that the region of memory can be read from and written to.
        ReadWrite = winnt::PAGE_READWRITE as isize,

        /// Specifies that the region of memory can be executed.
        ///
        /// Note that this implies `Read` as well on some architectures (e.g. x86).
        Execute = winnt::PAGE_EXECUTE as isize,

        /// Specifies that the region of memory can be read from and executed.
        ReadExecute = winnt::PAGE_EXECUTE_READ as isize,

    }

    /// Attempts to heap-allocate an array of the specified length.
    ///
    /// # Arguments
    /// * `count` The amount of pages to allocate.
    ///
    /// # Returns
    /// * The `Result` containing either the address of the first byte of the allocated memory, or
    ///   the `Error`.
    pub fn allocate(count: usize) -> io::Result<*mut usize> {
        let null = ptr::null::<raw::c_void>() as *mut raw::c_void;
        let length = count as SIZE_T * *PAGE_SIZE as SIZE_T;

        let flags = winnt::MEM_COMMIT; // TODO need MEM_RESERVE?
        let protection = winnt::PAGE_READWRITE; 

        let address = unsafe {
            kernel32::VirtualAlloc(null, length, flags, protection) as *mut usize
        };

        if address as usize == NULL { Err(Error::last_os_error()) } else { Ok(address) }
    }

    /// Attempts to deallocate the specified block of `Memory`.
    ///
    /// # Arguments
    /// * `memory` The `Memory` to deallocate.
    ///
    /// # Unsafety
    /// * `Memory` must not be accessed after deallocation.
    ///
    /// # Returns
    /// * `true` iff the call was succesful. `false` does not necessarily indicate it is still safe
    ///   to use `memory`.
    pub unsafe fn deallocate(memory: &mut Memory) -> bool {
        kernel32::VirtualFree(memory.address() as LPVOID, 0, winnt::MEM_RELEASE) == TRUE
    }

}

/// Contains unix-specific memory functions used by the JIT.
#[cfg(unix)]
mod memory {

    extern crate libc;
    extern crate sysconf;

    use self::libc::{c_int, c_void};
    use self::sysconf::{sysconf, SysconfVariable};

    use std::{io, ptr};
    use std::io::Error;

    use super::Memory;

    /// The file descriptor used for anonymous memory mapping.
    const ANONYMOUS_FILE_DESCRIPTOR: c_int = -1;

    /// The `c_int` value indicating a successful call to `mprotect`, `mmap`, or `munmap`.
    const SUCCESS: c_int = 0;

    lazy_static! {
        /// The size of an entry in the page table, in bytes.
        pub static ref PAGE_SIZE: usize = {
            let result = sysconf(SysconfVariable::ScPagesize);
            if let Err(error) = result {
                panic!("Failed to get page size: {:?}.", error);
            }

            result.unwrap() as usize
        };
    }

    /// A state of protection for a region of memory, on a unix machine.
    #[derive(Clone, Copy, Eq, Hash, PartialEq)]
    pub enum MemoryProtection {

        /// Specifies that the region of memory can be read from.
        Read = libc::PROT_READ as isize,

        /// Specifies that the region of memory can be written to.
        Write = libc::PROT_WRITE as isize,

        /// Specifies that the region of memory can be read from and written tp.
        ReadWrite = (libc::PROT_READ | libc::PROT_WRITE) as isize,

        /// Specifies that the region of memory can be executed.
        ///
        /// Note that this implies `Read` as well on some architectures (e.g. x86).
        Execute = libc::PROT_EXEC as isize,

        /// Specifies that the region of memory can be read from and executed.
        ReadExecute = (libc::PROT_READ | libc::PROT_EXEC) as isize,

    }

    /// Applies the specified `MemoryProtection` to the specified region of memory.
    ///
    /// The `MemoryProtection` will be applied to all of the memory in the interval
    /// [address, address + length - 1].
    ///
    /// # Arguments
    /// * `memory` The `Memory` to protect.
    /// * `protection` The `MemoryProtection` to apply. Existing protections will be revoked.
    ///
    /// # Returns
    /// `true` iff the call was successful.
    pub fn protect(memory: &mut Memory, protection: MemoryProtection) -> bool {
        let address = memory.address() as *mut c_void;
        unsafe { libc::mprotect(address, memory.length(), protection as c_int) == SUCCESS }
    }

    /// Attempts to heap-allocate an array of the specified length.
    ///
    /// # Arguments
    /// * `count` The count of pages to allocate.
    ///
    /// # Returns
    /// * The `Result` containing either the address of the first byte of the allocated memory, or
    ///   the `Error`.
    pub fn allocate(count: usize) -> io::Result<*mut usize> {
        let null = ptr::null::<c_void>() as *mut c_void;
        let length = count * *PAGE_SIZE;

        let protection = libc::PROT_READ | libc::PROT_WRITE; 
        let flags = libc::MAP_ANONYMOUS | libc::MAP_PRIVATE;

        let address = unsafe {
            libc::mmap(null, length, protection, flags, ANONYMOUS_FILE_DESCRIPTOR, 0)
        };

        if address == libc::MAP_FAILED { Err(Error::last_os_error()) } else { Ok(address as *mut usize) }
    }

    /// Attempts to deallocate the specified block of `Memory`.
    ///
    /// # Arguments
    /// * `memory` The `Memory` to deallocate.
    ///
    /// # Unsafety
    /// * `Memory` must not be accessed after deallocation.
    ///
    /// # Returns
    /// * `true` iff the call was succesful. `false` does not necessarily indicate it is still safe
    ///   to use `memory`.
    pub unsafe fn deallocate(memory: &mut Memory) -> bool {
        libc::munmap(memory.address() as *mut c_void, memory.length()) == SUCCESS
    }

}

#[cfg(test)]
mod test {

    use std::error::Error;

    use super::*;

    #[test]
    fn allocate() {
        if let Err(error) = Memory::allocate(1) {
            panic!("Failed to allocate memory: {}.", error.description());
        }
    }

}
