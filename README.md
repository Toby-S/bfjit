
# bfjit

A JIT compiler for brainfuck, in Rust.

## Building

Requires cargo, the Rust package manager. Once you've cloned this repository,
just run:

```
bfjit$ cargo build
```

# License

See [LICENSE](LICENSE).

